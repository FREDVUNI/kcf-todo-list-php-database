<?php
require_once("../database/database.php");

if(isset($_POST["todoInput"]) && !empty($_POST["todoInput"])){
    $todo_item = $_POST["todoInput"];

    if(strlen($todo_item) > 25){
        header("Location: ../index.php?error=Todo item shouldnot be more than 25 words.");
    }

    $stmt = $conn->prepare("INSERT INTO tasks (task) VALUES (?)");
    $stmt->bind_param("s", $todo_item);
    $stmt->execute();

    header("Location: ../index.php?success=The item has been added.");
} else {
    header("Location: ../index.php?error=Todo item should not be empty.");
}
?>
