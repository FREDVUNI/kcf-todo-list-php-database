<?php
require_once("../database/database.php");

$id = isset($_GET["id"]) ? $_GET["id"] : null;

if (!$id ) {
    header("Location:../index.php?error=Invalid id.");
    exit();
}

$sql = "DELETE FROM tasks WHERE id=$id";
$result = $conn->query($sql);

if ($result) {
    header("Location:../index.php?success=Item has been deleted.");
} else {
    header("Location:../index.php?error=There was an error deleting task.");
}
?>
