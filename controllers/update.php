<?php
require_once("../database/database.php");

$id = isset($_POST["id"]) ? $_POST["id"] : null;
$task = isset($_POST["todoInput"]) ? $_POST["todoInput"] : null;

if (!$id || !$task) {
    header("Location:../edit.php?error=Invalid input or id.");
    exit();
}

$sql = "UPDATE tasks SET task='$task' WHERE id=$id";
$result = $conn->query($sql);

if ($result) {
    header("Location:../index.php");
} else {
    header("Location:../edit.php?error=There was an error updating task.");
}
?>
